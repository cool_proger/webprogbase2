const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const models_help = require('../modules/models_help');

const Table = require("./table");
const User = require("./user");

const PlaceSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50,
    },
    description: {
        type:String,
        required: false,
        minlength: 3,
        maxlength: 300,
        trim: true,
    },
    small_pic: {
        type: String,
        required: true,
        // match: models_help.RegExp.url,
        trim: true,
        default: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
    },
    big_pic: {
        type: String,
        required: true,
        // match: models_help.RegExp.url,
        trim: true,
        default: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
    },
    // tables: [{
    //     type: ObjectId,
    //     ref: "Table",
    // }],
    // rating: Number,
    staff: [{
        type: ObjectId,
        ref: "User",
    }],
});

class Place {
    static getAll(conditions, clear) {
        const data = Place.getModel().find(conditions);
        if (clear) {
            return data.then(data => {
                return Place.getClearDataOf(data);
            });
        }
        return data;
    }
    static create(data) {
        return Place.getModel().create(data);
    }
    static getById(id, clear) {
        const data = Place.getModel().findById(id);
        if (clear) {
            return data.then(data => {
                return Place.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByConditions(conditions, clear) {
        const data = Place.getModel().findOne(conditions);
        if (clear) {
            return data.then(data => {
                return Place.getClearDataOf(data);
            });
        }
        return data;
    }
    static updateById(id, data) {
        return Place.getModel().findByIdAndUpdate(id, data);
    }
    static deleteById(id) {
        return Place.getModel().findByIdAndDelete(id);
    }
    static getTemplate() {
        return {
            name: "",
            description: "",
            small_pic: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
            big_pic: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
            // tables: [],
            staff: [],
        };
    }
    static getClearDataOf(data) {
        if (Array.isArray(data)) {
            const arrOfPromises = data.map((value) => {
                return Place.getClearDataOf(value);
            });
            return Promise.all(arrOfPromises);
        }

        const obj = {
            id: data._id,
            name: data.name,
            description: data.description,
            small_pic: data.small_pic,
            big_pic: data.big_pic,
            staff: [],
        };

        const arrOfPromises = data.staff.map((value) => {
            return User.getById(value)
            .then(data => User.getSmallClearDataOf(data));
        });

        return Promise.all(arrOfPromises)
        .then(() => Promise.resolve(obj));
    }
    static getSmallClearDataOf(data) {
        return {
            id: data._id,
            name: data.name,
        };
    }
    static getModel() {
        return mongoose.model('Place', PlaceSchema);
    }
    static getSchema() {
        return PlaceSchema;
    }
}

module.exports = Place;