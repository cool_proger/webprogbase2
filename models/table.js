const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
// const ObjectId = mongoose.Schema.Types.ObjectId;

const models_help = require('../modules/models_help');

const Place = require("./place");
const Order = require("./order");

const TableSchema = new Schema({
    number: {
        type: Number,
        min: 0,
        required: true,
        // default: () => Table.getModel().count(),
        default: 0,
        // unique: true,
    },
    place: {
        type: ObjectId,
        ref: "Place",
    },
    // orders: [{
    //     type: ObjectId,
    //     ref: "Order",
    // }],
});

class Table {
    static getAll(conditions, clear) {
        const data = Table.getModel().find(conditions);
        if (clear) {
            return data.then(data => {
                return Table.getClearDataOf(data);
            });
        }
        return data;
    }
    static create(data) {
        return Table.getModel().create(data);
    }
    static getById(id, clear) {
        const data = Table.getModel().findById(id);
        if (clear) {
            return data.then(data => {
                return Table.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByConditions(conditions, clear) {
        const data = Table.getModel().findOne(conditions);
        if (clear) {
            return data.then(data => {
                return Table.getClearDataOf(data);
            });
        }
        return data;
    }
    static updateById(id, data) {
        return Table.getModel().findByIdAndUpdate(id, data);
    }
    static deleteById(id) {
        return Table.getModel().findByIdAndDelete(id);
    }
    static getTemplate(place_id) {
        return {
            place: ObjectId(place_id),
            number: 0,
            // orders: [],
        };
    }
    static getClearDataOf(data) {
        if (Array.isArray(data)) {
            const arrOfPromises = data.map((value) => {
                return Table.getClearDataOf(value);
            });
            return Promise.all(arrOfPromises);
        }

        // if (Array.isArray(data)) {
        //     const arrOfPromises = [];
        //     for (let i in data) {
        //         arrOfPromises[i] = Table.getClearDataOf(data[i]);
        //     }
        //     return Promise.all(arrOfPromises);
        // }

        const obj = {
            id: data._id,
            number: data.number,
            place: data.place,
        };
        // return Place.getById(data.place)
        // .then(place => {
        //     console.log("PLACE" + place);
        //     obj.place = Place.getSmallClearDataOf(place);
        //     return Promise.resolve(obj);
        // });
        return Promise.resolve(obj);
    }
    static getSmallClearDataOf(data) {
        return {
            id: data._id,
            number: data.number,
        };
    }
    static getModel() {
        return mongoose.model('Table', TableSchema);
    }
    static getSchema() {
        return TableSchema;
    }
}

module.exports = Table;