const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const models_help = require('../modules/models_help');

const UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true,
        minlength: 3,
        maxlength: 20
    },
    role: {
        type: String,
        enum: ["system_admin", "place_admin", "waiter", "kitchen", "client"],
        required: true, 
        default: "client",
        trim: true,
    },
    first_name: {
        type: String,
        required: false,
        trim: true,
        minlength: 2,
        maxlength: 20,
    },
    last_name: {
        type: String,
        required: false,
        trim: true,
        minlength: 2,
        maxlength: 20,
    },
    email: {
        type: String,
        required: false,
        match: models_help.RegExp.email,
        trim: true,
        maxlength: 25,
        unique: true,
    },
    registration_date: {
        type: Date,
        required: true,
        default: Date.now,
        // default: () => new Date().toISOString().slice(0, 10), // "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
    },
    small_pic: {
        type: String,
        required: true,
        match: models_help.RegExp.url,
        trim: true,
        default: 'https://res.cloudinary.com/cyberscript67/image/upload/v1545331347/default.png',
    },
    password_hesh: {
        type: String,
        required: false,
        trim: true,
        minlength: 8,
    },
});

class User {
    static getAll(conditions, clear) {
        const data = User.getModel().find(conditions);
        if (clear) {
            return data.then(data => {
                return User.getClearDataOf(data);
            });
        }
        return data;
    }
    static create(data) {
        return User.getModel().create(data);
    }
    static getById(id, clear) {
        const data = User.getModel().findById(id);

        if (clear) {
            return data.then(data => {
                // console.log(data); //
                return User.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByLoginAndPassword(username, password_hesh, clear) {
        const data = User.getModel().findOne({
            username: username,
            password_hesh: password_hesh,
        });
        if (clear) {
            return data.then(data => {
                return User.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByConditions(conditions, clear) {
        const data = User.getModel().findOne(conditions);
        if (clear) {
            return data.then(data => {
                return User.getClearDataOf(data);
            });
        }
        return data;
    }
    static updateById(id, data) {
        return User.getModel().findByIdAndUpdate(id, data);
    }
    static deleteById(id) {
        return User.getModel().findByIdAndDelete(id);
    }
    static getTemplate() {
        return {
            username: "",
            role: "client",
            first_name: "",
            last_name: "",
            email: "",
            registration_date: new Date().toISOString().slice(0, 10), // "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
            small_pic: "https://res.cloudinary.com/cyberscript67/image/upload/v1545331347/default.png",
            password_hesh: "",
        };
    }
    static createTemporaryUser() {
        const generatedValue = models_help.GenerateUsername();
        let user = {
            username: generatedValue,
            role: "client",
            // first_name: "",
            // last_name: "",
            // email: "",
            registration_date: new Date().toISOString().slice(0, 10),// "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
            small_pic: "https://res.cloudinary.com/cyberscript67/image/upload/v1545331347/default.png",
            password_hesh: crypto.createHash('md5').update(generatedValue).digest('hex'),
        };
        const data = User.getModel().create(user);
        return data;
    }
    static getClearDataOf(data) {
        if (Array.isArray(data)) {
            const arrOfPromises = data.map((value) => {
                return User.getClearDataOf(value);
            });
            return Promise.all(arrOfPromises);
        }

        const result = {
            id: data._id,
            username: data.username,
            first_name: data.first_name,
            last_name: data.last_name,
            // role: data.role,
            email: data.email,
            registration_date: data.registration_date.toISOString().slice(0, 10),
            small_pic: data.small_pic,
        };
        switch(data.role) {
            case "client": {
                result.role = { client: true }; break;
            }
            case "kitchen": {
                result.role = { kitchen: true }; break;
            }
            case "waiter": {
                result.role = { waiter: true }; break;
            }
            case "place_admin": {
                result.role = { place_admin: true }; break;
            }
            case "system_admin": {
                result.role = { system_admin: true }; break;
            }
            // default: return Promise.reject("Role isn't correct! Role = " + data.role);
        }
        return Promise.resolve(result);
    }
    static getSmallClearDataOf(data) {
        return {
            id: data._id,
            first_name: data.first_name,
            last_name: data.last_name,
            role: data.role,
        };
    }
    static getModel() {
        return mongoose.model('User', UserSchema);
    }
    static getSchema() {
        return UserSchema;
    }
}

module.exports = User;