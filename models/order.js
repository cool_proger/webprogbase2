const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const models_help = require('../modules/models_help');

const Item = require("./item");
const User = require("./user");
const Table = require("./table");

const OrderSchema = new Schema({
    stage: {
        type: Number,
        min: 0,
        max: 4,
        required: true,
    },
    number: {
        type: Number,
        min: 0,
        required: true,
        // default: () => Order.getModel().count(),
        default: 0,
        // unique: true,
    },
    items: [{
        type: ObjectId,
        ref: "Item",
    }],
    price: {
        type: Number,
        required: true,
        min: 0,
        default: 0,
    },
    client: {
        type: ObjectId,
        ref: "User",
        required: true,
    },
    waiter: {
        type: ObjectId,
        ref: "User",
    },
    date: {
        type: Date,
        required: true,
        default: Date.now,
        // default: () => new Date().toISOString().slice(0, 10), // "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
    },
    table: {
        type: ObjectId,
        ref: "Table",
    }
});

class Order {
    static getAll(conditions, clear) {
        const data = Order.getModel().find(conditions);
        if (clear) {
            return data.then(data => {
                return Order.getClearDataOf(data);
            });
        }
        return data;
    }
    static create(data) {
        data.number = Order.next_number;
        return Order.getModel().create(data)
        .then(data => {
            console.log("OrderNum++");
            Order.next_number++;
            return data;
        });
    }
    static getById(id, clear) {
        const data = Order.getModel().findById(id);
        if (clear) {
            return data.then(data => {
                return Order.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByConditions(conditions, clear) {
        const data = Order.getModel().findOne(conditions);
        if (clear) {
            return data.then(data => {
                return Order.getClearDataOf(data);
            });
        }
        return data;
    }
    static updateById(id, data) {
        return Order.getModel().findByIdAndUpdate(id, data);
    }
    static deleteById(id) {
        return Order.getModel().findByIdAndDelete(id);
    }
    static getTemplate(table_id, client_id) {
        return {
            stage: 0,
            number: Order.next_number,
            items: [],
            price: 0,
            client: client_id,
            waiter: null,
            date: new Date().toISOString().slice(0, 10),
            table: table_id,
        };
    }
    static getClearDataOf(data) {
        if (Array.isArray(data)) {
            const arrOfPromises = data.map((value) => {
                return Order.getClearDataOf(value);
            });
            return Promise.all(arrOfPromises);
        }

        const obj =  {
            id: data._id,
            stage: data.stage,
            number: data.number,
            price: data.price,
            client: data.client,
            waiter: data.waiter,
            date: data.date.toISOString().slice(0, 10),
            table: data.table,
            items: [],
        };

        const arrOfPromises = data.items.map((value) => {
            return Item.getById(value)
            .then(data => obj.items.push(Item.getSmallClearDataOf(data)));
        });

        // const clientProm = User.getById(data.client)
        // .then(data => obj.client = User.getSmallClearDataOf(data));
        // arrOfPromises.push(clientProm);

        // if (data.waiter) {
        //     const waiterProm = User.getById(data.waiter)
        //     .then(data => obj.waiter = User.getSmallClearDataOf(data));
        //     arrOfPromises.push(waiterProm);
        // }

        // const tableProm = Table.getById(data.table)
        // .then(data => obj.table = Table.getSmallClearDataOf(data));
        // arrOfPromises.push(tableProm);
        
        return Promise.all(arrOfPromises)
        .then(() => {
            return obj;
        });
    }
    static getModel() {
        return mongoose.model('Order', OrderSchema);
    }
    static getSchema() {
        return OrderSchema;
    }
}

Order.getModel().count().then(data => {
    Order.next_number = data + 1;
});

module.exports = Order;