const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const models_help = require('../modules/models_help');

const Place = require("./place");

const ItemSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50,
    },
    description: {
        type:String,
        required: false,
        minlength: 3,
        maxlength: 300,
        trim: true,
    },
    // weight: {
    //     // type: Number,
    //     required: false,
    //     // min: 0,
    // },
    category: {
        // type: ObjectId,
        // ref: 'CategorySchema',
        required: true,
        type: String,
        enum: ["pizza", "sushi", "burgers", "kebabs", "steaks", "salads", "pasta", "soups", "desserts", "drinks"],
    },
    small_pic: {
        type: String,
        required: true,
        match: models_help.RegExp.url,
        trim: true,
        default: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
    },
    design_date: {
        type: Date,
        required: true,
        default: Date.now,
        // default: () => new Date().toISOString().slice(0, 10), // "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
    },
    price: {
        type: Number,
        min: 0,
        required: true,
        default: 0,
    },
    place: {
        type: ObjectId,
        ref: "Place",
        required: true,
    },
});

class Item {
    static getAll(conditions, clear) {
        const data = Item.getModel().find(conditions);
        if (clear) {
            return data.then(data => {
                return Item.getClearDataOf(data);
            });
        }
        return data;
    }
    static create(data) {
        return Item.getModel().create(data);
    }
    static getById(id, clear) {
        const data = Item.getModel().findById(id);
        if (clear) {
            return data.then(data => {
                // console.log(data);
                return Item.getClearDataOf(data);
            });
        }
        return data;
    }
    static getByConditions(conditions, clear) {
        const data = Item.getModel().findOne(conditions);
        if (clear) {
            return data.then(data => {
                return Item.getClearDataOf(data);
            });
        }
        return data;
    }
    static updateById(id, data) {
        return Item.getModel().findByIdAndUpdate(id, data);
    }
    static deleteById(id) {
        return Item.getModel().findByIdAndDelete(id);
    }
    static getTemplate(place_id) {
        return {
            name: "",
            description: "",
            // weight: 0,
            category: "",
            small_pic: "https://res.cloudinary.com/cyberscript67/image/upload/v1548333644/default-food.png",
            design_date: new Date().toISOString().slice(0, 10), // "YYYY-MM-DDTHH:mm:ss.sssZ" --> "YYYY-MM-DD"
            price: 0,
            place: place_id,
        };
    }
    static getClearDataOf(data) {
        if (Array.isArray(data)) {
            const arrOfPromises = data.map((value) => {
                return Item.getClearDataOf(value);
            });
            return Promise.all(arrOfPromises);
        }

        const item = {
            id: data._id,
            name: data.name,
            description: data.description,
            // category: data.category,
            small_pic: data.small_pic,
            design_date: data.design_date.toISOString().slice(0, 10),
            price: data.price,
            place: null,
        };
        switch(data.category) {
            case "pizza": item.category = { pizza: true }; break;
            case "sushi": item.category = { sushi: true }; break;
            case "burgers": item.category = { burgers: true }; break;
            case "kebabs": item.category = { kebabs: true }; break;
            case "steaks": item.category = { steaks: true }; break;
            case "salads": item.category = { salads: true }; break;
            case "pasta": item.category = { pasta: true }; break;
            case "other": item.category = { other: true }; break;
            default: return Promise.reject("Category is not correct!");
        }

        return Place.getById(data.place)
        .then(data => {
            item.place = Place.getSmallClearDataOf(data);
        })
        .then(() => Promise.resolve(item));
    }
    static getSmallClearDataOf(data) {
        return {
            id: data._id,
            name: data.name,
            price: data.price,
        };
    }
    static getModel() {
        return mongoose.model('Item', ItemSchema);
    }
    static getSchema() {
        return ItemSchema;
    }
}

module.exports = Item;