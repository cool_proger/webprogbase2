const express = require('express');
const cloudinary = require('../cloudinary');

const page_render = require('../modules/page_render');
const check_rights = require('../modules/check_rights');

const Place = require('../models/place');
const Table = require('../models/table');

const router = express.Router();

router.get('/', (req, res) => {
    Place.getAll({}, true) // @todo normal way
    .then(data => {
        res.render('places', page_render.places(req.user, data));
    })
    .catch(err => {
        res.end(err.toString());
    });
});

router.get('/new', check_rights.checkAdmin, (req, res) => {
    res.render('place_edit', page_render.place_new(req.user));
});

router.post('/new', check_rights.checkAdmin, (req, res) => {
    const place = {
        // name: req.body.name,
        // description: req.body.description,
        staff: [],
    };
    if (req.body) {
        if (req.body.name) {
            place.name = req.body.name;
        }
        if (req.body.description) {
            place.description = req.body.description;
        }
    }    
    const arrOfPromises = [];
    if (req.files) {
        if (req.files.small_pic) {
            const prom = cloudinary.uploadFile(req.files.small_pic)
            .then(img => {
                place.small_pic = img.url;
            });
            arrOfPromises.push(prom);
        }
        if (req.files.big_pic) {
            const prom = cloudinary.uploadFile(req.files.big_pic)
            .then(img => {
                place.big_pic = img.url;
            });
            arrOfPromises.push(prom);
        }
    }
    Promise.all(arrOfPromises)
    .then(() => Place.create(place))
    .then(data => res.redirect(`/places/${data._id}`))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    Place.getById(id, true)
    .then(data => {
        return res.render('place', page_render.place(req.user, data));
    })
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/edit', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Place.getById(id, true)
    .then(data => {
        return res.render('place_edit', page_render.place_edit(req.user, data));
    })
    .catch(err => res.end(err.toString()));
});

router.post('/:id/edit', check_rights.checkAdmin, (req, res) => {
    const place = {
        // name: req.body.name,
        // description: req.body.description,
        staff: [],
    };
    if (req.body) {
        if (req.body.name) {
            place.name = req.body.name;
        }
        if (req.body.description) {
            place.description = req.body.description;
        }
    }   

    const arrOfPromises = [];
    if (req.files) {
        if (req.files.small_pic) {
            const prom = cloudinary.uploadFile(req.files.small_pic)
            .then(img => {
                place.small_pic = img.url;
            });
            arrOfPromises.push(prom);
        }
        if (req.files.big_pic) {
            const prom = cloudinary.uploadFile(req.files.big_pic)
            .then(img => {
                place.big_pic = img.url;
            });
            arrOfPromises.push(prom);
        }
    }
    Promise.all(arrOfPromises)
    .then(() => Place.updateById(req.params.id, place))
    .then(data => res.redirect(`/places/${data._id}`))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id/delete', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Place.deleteById(id)
    .then(() => res.redirect('/places'))
    .catch(err => res.end(err.toString()));
});

router.get('/:id/tables', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Table.getAll({
        place: id,
    }, true)
    .then(data => {
        // return Place.getById(id)
        // .then(place => {
        //     res.render('tables', page_render.place_tables(req.user, data, 
        //         Place.getSmallClearDataOf(place)
        //     ));
        // });
        console.log("DATATTTTA");
        console.log(data);
        res.render('tables', page_render.place_tables(req.user, data, id));
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/add_table', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Table.create({
        place: id,
        number: 0,
        // orders: [],
    })
    .then(data => {
        console.log("GET" + data);
        res.redirect(`/places/${id}/tables`);
    })
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/tables/:id/delete', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Table.deleteById(id)
    .then(data => res.redirect(`/places/${data.place}/tables`))
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});


module.exports = router;