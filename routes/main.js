const express = require('express');
const url = require('url');

const page_render = require('../modules/page_render');

const itemsRouter = require('./items'); 
const placesRouter = require('./places'); 
const authRouter = require('./auth'); 
const ordersRouter = require('./orders');
const usersRouter = require('./users');

const Item = require('../models/item');
const Table = require("../models/table");
const User = require("../models/user");
const Order = require("../models/order");

const router = express.Router();

router.get('/', (req, res) => {
    // Item.getAll({}, true) // @todo normal way
    // .then(data => res.render('index', page_render.index(req.user, data)))
    // .catch(err => res.end(err.toString()));

    res.render('index', page_render.index(req.user));
});

router.get('/news', (req, res) => {
    res.render('news', page_render.news(req.user));
});

router.get('/about', (req, res) => {
    res.render('about', page_render.about(req.user));
});

router.get('/scan', (req, res) => {
    res.render('scan', page_render.scan(req.user));
});

router.get('/scan/guest/:table_id', (req, res) => {
    const order = Order.getTemplate(req.params.table_id, req.user.id);
    Order.create(order)
    .then(data => {
        req.session.order_id = data._id;
        return Table.getById(req.params.table_id, true)
        .then(data => {
            req.session.place_id = data.place;
        });
    })
    .then(() => {
        req.user = User.createTemporaryUser();
        res.redirect(url.format({
            pathname:"/items",
            query: {
               "place": req.session.place_id,
            }
        }));
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

// router.get('/scan/auth/:table_id', (req, res) => {
//     const order = Order.getTemplate(req.params.table_id, req.user.id);
//     Order.create(order)
//     .then(data => {
//         req.session.order_id = data._id;
//         return Table.getById(req.params.table_id, true)
//         .then(data => {
//             req.session.place_id = data.place;
//             res.redirect("/auth" + req.session.place_id);
//         });
//     })
//     .catch(err => {
//         console.log(err);
//         res.status(404);
//         res.end(err.toString());
//     });
// });

router.get('/scan/success/:table_id', (req, res) => {
    const order = Order.getTemplate(req.params.table_id, req.user.id);
    Order.create(order)
    .then(data => {
        req.session.order_id = data._id;
        return Table.getById(req.params.table_id, true)
        .then(data => {
            req.session.place_id = data.place;
            res.redirect("/items?place=" + req.session.place_id);
        });
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

// //
// router.get('/tables/:id', (req, res) => {
//     Table.getById(req.params.id, true)
//     .then(data => {
        
//     })
//     .then(() => res.redirect("/auth"));
// });


/**
 *  Aditional routers
 */
router.use('/items', itemsRouter);
router.use('/places', placesRouter);
router.use('/auth', authRouter);
router.use('/orders', ordersRouter);
router.use('/users', usersRouter);

module.exports = router;