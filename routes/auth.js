const express = require('express');
const passport = require('passport');
const url = require('url');
const crypto = require('crypto');
const page_render = require('../modules/page_render');
const check_rights = require('../modules/check_rights');

const router = express.Router();

const User = require('../models/user');
const Order = require("../models/order");

router.get('/', check_rights.checkNotAuth, (req, res) => {
    res.render('auth', page_render.auth());
});

// router.post('/login', check_rights.checkNotAuth, (req, res) => {
//     // do stuff 
//     // // get user id
//     // const user_id = 444;
//     // res.redirect(`users/${user_id}`);
//     res.redirect('/');
// });

router.post('/login', passport.authenticate('local', { failureRedirect: '/auth/login?error=Incorrect+data,+try+again!' }), (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user._id); 
        return;
    }

    let user_id;
    User.getByConditions({
        username: req.body.username,
    })
    .then(data => user_id = data._id)
    .then(() => Order.create({
        items: [],
        client: user_id,
        table: req.session.table_id,
    }))
    .then(data => req.session.order_id = data._id)
    .then(() =>  res.redirect('/'));
});

router.post('/register', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user._id); 
        return;
    }

    // @todo server validation
    if (req.body.password !== req.body.confirm_password) {
        // res.render('register', {
        //     error: "Paswords doesn't match each other!"
        // });
        res.redirect(url.format({
            pathname:"/auth/register",
            query: {
               "error": "Paswords doesn't match each other!",
            }
        }));
    } else {
        const user = {
            username: req.body.username,
            role: "system_admin", // @todo
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            // registration_date: new Date().toISOString(),
            // small_pic: data.small_pic,
            password_hesh: crypto.createHash('md5').update(req.body.password).digest('hex'),
        };
        User.create(user)
        .then(() => {
            // res.redirect(`/users/${data.id}`);
            res.redirect('/auth');
        })
        .catch(err => res.end("Error: " + err.toString()));
    }
});

router.get('/logout', check_rights.checkAuth, (req, res) => {
    req.logout();   
    res.redirect('/');
});


module.exports = router;