const express = require('express');
const cloudinary = require('../cloudinary');

const page_render = require('../modules/page_render');
const check_rights = require('../modules/check_rights');
// const update_help = require('../modules/update_help');

const Item = require('../models/item');
const Place = require('../models/place');

const router = express.Router();

router.get('/', (req, res) => {
    if (Boolean(req.query.list)) {
        // const page = Number(req.query.page ? req.query.page : 1);

        delete req.query.list;
        Item.getAll(req.query, true)
        .then(data => {
            // console.log(req.session.order_id);
            const userData = req.user;
            userData.order_id = req.session.order_id;
            userData.place_id = req.session.place_id;

            res.render('items', page_render.items(userData, data, req.query.category));
        })
        .catch(err => {
            res.end(err.toString());
        });
        
    } else {
        const userData = req.user;
            userData.order_id = req.session.order_id;
            userData.place_id = req.session.place_id;
        res.render('categories', page_render.categories(userData));
    }
});

router.get('/new', check_rights.checkAdmin, (req, res) => {
    Place.getById(req.query.place)
    .then(data => {
        const item = Item.getTemplate(req.query.place);
        item.category = {};
        item.place = Place.getSmallClearDataOf(data);
        res.render('item_edit', page_render.item_new(req.user, item));
    });
});

router.post('/new', check_rights.checkAdmin, (req, res) => {
    // console.log(req.query.place);
    const item = {
        place: req.query.place,
    };
    if (req.body) {
        if (req.body.name) {
            item.name = req.body.name;
        }
        if (req.body.description) {
            item.description = req.body.description;
        }
        if (req.body.category) {
            item.category = req.body.category;
        }
        if (req.body.design_date) {
            item.design_date = req.body.design_date;
        }
        if (req.body.price) {
            item.price = req.body.price;
        }
        if (req.body.registration_date) {
            item.registration_date = req.body.registration_date;
        }
    }    
    let prom = Promise.resolve();
    if (req.files && req.files.small_pic) {
        const smallImgFileObject = req.files.small_pic; 
        prom = cloudinary.uploadFile(smallImgFileObject)
        .then(img => item.small_pic = img.url);
    }
    // console.log(item);
    prom.then(() => Item.create(item))
    .then(data => res.redirect(`/items/${data._id}`))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id', (req, res) => {
    const id = req.params.id;
    Item.getById(id, true)
    .then(data => {
        // console.log(data);
        res.render('item', page_render.item(req.user, data));
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/edit', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Item.getById(id, true)
    .then(data => res.render('item_edit', page_render.item_edit(req.user, data)))
    .catch(err => res.end(err.toString()));
});

router.post('/:id/edit', check_rights.checkAdmin,  (req, res) => {
    const item = {
        place: req.query.place,
    };
    if (req.body) {
        if (req.body.name) {
            item.name = req.body.name;
        }
        if (req.body.description) {
            item.description = req.body.description;
        }
        if (req.body.category) {
            item.category = req.body.category;
        }
        if (req.body.design_date) {
            item.design_date = req.body.design_date;
        }
        if (req.body.price) {
            item.price = req.body.price;
        }
        if (req.body.registration_date) {
            item.registration_date = req.body.registration_date;
        }
    }    
    let prom = Promise.resolve();
    if (req.files && req.files.small_pic) {
        const smallImgFileObject = req.files.small_pic; 
        prom = cloudinary.uploadFile(smallImgFileObject)
        .then(img => item.small_pic = img.url);
    }
    console.log(item.category);
    prom.then(() => Item.updateById(req.params.id, item))
    .then(data => res.redirect(`/items/${data._id}`))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id/delete', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Item.deleteById(id)
    .then(() => res.redirect('/items'))
    .catch(err => res.end(err.toString()));
});


module.exports = router;