const express = require('express');

const page_render = require('../modules/page_render');
const check_rights = require('../modules/check_rights');
// const update_help = require('../modules/update_help');

const Order = require('../models/order');
const Item = require('../models/item');
const User = require('../models/user');

const router = express.Router();

router.get('/', check_rights.checkAdmin, (req, res) => {
    Order.getAll({}, true) // @todo normal way
    .then(data => {
        res.render('orders', page_render.orders(req.user, data));
    })
    .catch(err => {
        console.log(err);
        res.end(err.toString());
    });
});

router.get('/:id/new', check_rights.checkAuth, (req, res) => { // not creation, just publication (confirmation) - changing status
    Order.updateById(req.params.id, {
        status: 1,
    })
    .then(() => res.redirect(`/orders/${req.params.id}`));

    // const order = Order.getTemplate(req.session.table_id, req.user.id);    
    // Promise.resolve(order)
    // .then(Order.create)
    // .then(data => res.redirect(`/orders/${data._id}`))
    // .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id', check_rights.checkAuth, (req, res) => {
    const id = req.params.id;
    Order.getById(id, true)
    .then(data => {
        res.render('order', page_render.order(req.user, data));
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/add/:item_id', check_rights.checkAuth, (req, res) => {
    const id = req.params.id;
    const item_id = req.params.item_id;

    Order.getById(id)
    .then(order => {
        return Item.getById(item_id)
        .then(data => {
            // order.$push = {
            //     items: item_id,
            // };
            // { $push: { board: 10 } }
            order.items.push(item_id);
            order.price += data.price;
            // return Order.updateById(id, order);
            return order.save();
        });
        // .then(() => console.log(order));

    })
    
    // Order.updateById(id, {
    //     items: [
    //         item_id,
    //     ],
    // })
    .then(() => res.redirect(`/items/${item_id}`))
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });

    // let order;
    // Order.getById(id)
    // .then(data => order = data)
    // .then(() => Item.getById(item_id))
    // .then(data => {
    //     order.items.push(data);
    //     return order.save();
    // })
    // // .then(() => Order.updateById(id, order))
    // .then(() => res.redirect(`/items/${item_id}`))
    // .catch(err => {
    //     res.status(404);
    //     res.end(err.toString());
    // });
});

router.get('/:id/status/:status_id', check_rights.checkAdmin, (req, res) => {
    const status_id = req.params.status_id;
    Order.updateById(req.params.id, {
        status: status_id,
    })
    .then(() => {
        res.redirect(`/orders/${req.params.id}`);
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/take', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;

    if (req.user.role !== "waiter") {
        res.end("Error: you can't do it!");
    }
    Order.updateById(id, {
        waiter: req.user.id,
    })
    .then(() => {
        res.redirect(`/order/${id}`);
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
    
    // if (req.user.role === "waiter") {
    //     User.getById(id)
    //     .then(waiter => {
    //         return Order.getById(id)
    //         .then(order => {
    //             order.waiter = waiter;
    //             return Order.updateById(id, order)
    //             .then(data => {
    //                 res.redirect(`/order/${id}`);
    //             });
    //         });
    //     })
    //     .catch(err => {
    //         res.status(404);
    //         res.end(err.toString());
    //     });
    // }
});

router.get('/:id/compleate', check_rights.checkAuth, (req, res) => {
    console.log(req.user.role);
    switch(req.user.role) {
        case "client": {
            res.redirect(`/orders/${req.params.id}/status/1`);
            break;
        }
        case "kitchen": {
            res.redirect(`/orders/${req.params.id}/status/2`);
            break;
        }
        case "waiter": {
            res.redirect(`/orders/${req.params.id}/status/3`);
            break;
        }
        case "system_admin": case "place_admin": {
            res.redirect(`/orders/${req.params.id}/status/4`);
            break;
        }
        default: {
            res.end("Error: you can't do it!");
        }
    }
});

router.get('/:id/delete', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    Order.deleteById(id)
    .then(() => {
        res.redirect('/orders');
    })
    .catch(err => {
        console.log(err);
        res.status(404);
        res.end(err.toString());
    });
});

module.exports = router;