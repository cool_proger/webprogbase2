const express = require('express');

const page_render = require('../modules/page_render');
const check_rights = require('../modules/check_rights');

const User = require('../models/user');
const cloudinary = require('../cloudinary');
const crypto = require('crypto');

const router = express.Router();

router.get('/', check_rights.checkAuth, (req, res) => {
    User.getAll({}, true) // @todo rewrite getAll to return [] of User objects, not UserModel
    .then(data => {
        res.render('users', page_render.users(req.user, data));
    })
    .catch(err => {
        res.end(err.toString());
    });
});

router.get('/:id', check_rights.checkAuth, (req, res) => {
    const id = req.params.id;
    User.getById(id, true)
    .then(data => {
        res.render('user', page_render.user(req.user, data));
    })
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/:id/edit', check_rights.checkAdmin, (req, res) => {
    User.getById(req.params.id, true)
    .then(data => {
        res.render('user_edit', page_render.user_edit(req.user, data));
    })
    .catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});

router.post('/:id/edit', check_rights.checkAdmin, (req, res) => {
    const user = {
        // username: req.body.username,
        // first_name: req.body.first_name,
        // last_name: req.body.last_name,
        // role: req.body.role,
        // email: req.body.email,
        // // small_pic: data.url,
        // registration_date: Date(req.body.registration_date),
    };
    if (req.body) {
        if (req.body.username) {
            user.username = req.body.username;
        }
        if (req.body.first_name) {
            user.first_name = req.body.first_name;
        }
        if (req.body.last_name) {
            user.last_name = req.body.last_name;
        }
        if (req.body.role) {
            user.role = req.body.role;
        }
        if (req.body.email) {
            user.email = req.body.email;
        }
        if (req.body.registration_date) {
            user.registration_date = req.body.registration_date;
        }
        if (req.body.password || req.body.confirm_password) {
            if (req.body.password !== req.body.confirm_password) {
                res.end("Error: passwrord doesn't match!");
                return;
            } 
            user.password_hesh = crypto.createHash('md5').update(req.body.password).digest('hex');
        }
    }    
    let prom = Promise.resolve();
    if (req.files && req.files.small_pic) {
        const smallImgFileObject = req.files.small_pic; 
        prom = cloudinary.uploadFile(smallImgFileObject)
        .then(img => user.small_pic = img.url);
    }
    prom.then(() => User.updateById(req.params.id, user, {new: true}))
    .then(data => { 
        res.redirect(`/users/${data._id}`);
    })
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/:id/delete', check_rights.checkAdmin, (req, res) => {
    const id = req.params.id;
    User.deleteById(id)
    .then(() => res.redirect('/users'))
    .catch(err => res.end(err.toString()));
});

module.exports = router;