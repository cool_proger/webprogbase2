/* Require the npm modules */
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const mustache = require('mustache-express');
const mongoose = require('mongoose');
const crypto = require('crypto');
// const fs = require('fs-promise'); 

/* My assist modules */
// const page_render = require('./modules/page_render');

// cookies and sessions
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');

// const busboy = require('connect-busboy');

/* Require local files */
const config = require('./config');
const cloudinary = require('./cloudinary');
const mainRouter = require('./routes/main');
// const itemsRouter = require('./routes/items');
// const placesRouter = require('./routes/places');
// const usersRouter = require('./routes/users');
// const mainRouter = require('./routes/main');

const User = require("./models/user");
// let OrderItem = require('./models/item');

/* Initial */
const PORT = config.ServerPort;
const DB_URL = config.DataBaseURL;
// const fs_path = config.FileSystemPath;

mongoose.Promise = global.Promise;

const app = express();

// app.use(busboy()); 

app.use(express.static('public'));
app.engine('mst', mustache("./views/partials"));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));


// cookies and sessions
app.use(cookieParser());
app.use(session({
	secret: "Some_secret^string",
	resave: false,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

/* Auth functions and data */
// визначає, яку інформацію зберігати у Cookie сесії
passport.serializeUser((user, callback) => {
    // наприклад, зберегти у Cookie сесії id користувача
    callback(null, user._id);
});

// отримує інформацію (id) із Cookie сесії і шукає користувача, що їй відповідає
passport.deserializeUser((id, callback) => {
	// отримати користувача по id і викликати done(null, user);
    // при помилці викликати done(err, null)
    User.getById(id)
    .then(data => callback(null, data))
    .catch(err => callback(err));
});

// налаштування стратегії для визначення користувача, що виконує логін
// на основі його username та password
passport.use(new LocalStrategy((username, password, callback) => {
	// отримати користувача по його username і password і викликати done(null, user);
    // при помилці викликати done(err, null)
    // console.log("UN: " + username + " | PS: " + password); // @todo delete output
    const hashedPassword = crypto.createHash('md5').update(password).digest('hex');
    User.getByLoginAndPassword(username, hashedPassword)
    .then(data => {
        // console.log("Found user: " + data);
        callback(null, data);
    })
    .catch(err => callback(err));
}));

// function getUserData(user) {
//     if (user) {
//         const basic = {
//             isAuth: true,
//             isNotAuth: false,
//             username: user.data.login, // @todo add little ava pic
//         };
//         if (user.data.role === 1) {
//             return { ...basic, isGeneralUser: false, isAdmin: true };
//         } else {
//             return { ...basic, isGeneralUser: true, isAdmin: false };
//         }
//     } else {
//         return {
//             username: "Not_logined!",
//             isAuth: false,
//             isNotAuth: true,
//             isGeneralUser: false, 
//             isAdmin: false,
//         };
//     }
// }

/* Routers */
app.use('/', mainRouter);
// app.use('/auth', authRouter);

/* Connecting! */
mongoose.connect(DB_URL, {
    useNewUrlParser: true,
})
    .then(() => console.log("Database connected!"))
    .then(() => {
        app.listen(PORT, (err) => {
            if (err) {
                return new Error("App starting error:" + err.toString());
            }
        });
    })
    .then(() => console.log(`Server is listening on port ${PORT}!`))
    .catch(err => console.log(`Server error: ${err.toString()}`));

/* GET and POST */

// app.get('/', (req, res) => {
//     res.render('index', getUserData(req.user));
// });

// app.get('/about', (req, res) => {
//     res.render('about', getUserData(req.user));
// });

// app.get('/users', checkAdmin, (req, res) => {
//     User.getAll() // @todo rewrite getAll to return [] of User objects, not UserModel
//         .then(data => {
//             res.render('users', { users: data, ...getUserData(req.user)});
//         })
//         .catch(err => {
//             res.end(err.toString());
//         });
// });

// app.get('/users/:id', checkAuth, (req, res) => {
//     const id = req.params.id;

//     User.getById(id)
//         .then(data => {
//             const uisAdmin = data.data.role === 1 ? true : false;
//             res.render(`user`, { ...data.data, ...getUserData(req.user), 
//             userIsAdmin: uisAdmin, userIsGUser: !uisAdmin});
//         })
//         .catch(err => {
//             res.status(404);
//             res.end(err.toString());
//         });
// });

// app.post('/users/:id/edit', checkAdmin, (req, res) => {
//         User.updateRoleById(req.params.id, {
//             role: Number(req.body.role),
//         }).then(() => res.redirect('/users/' + req.params.id))
//         .catch(err => res.end("Error: " + err.toString()));
// });

// app.get('/items', checkAuth, (req, res) => {
//     const page = Number(req.query.page ? req.query.page : 1);
//     const nItemsOnOnePage = 5;

//     OrderItem.getAll() // @todo normal way
//         .then(data => {
//             const first = (page - 1) * nItemsOnOnePage;
//             if (first < 0 || data.length - 1 < first) {
//                 res.sendStatus(404);
//             } else {
//                 const pagesNum = Math.ceil(data.length / nItemsOnOnePage);
//                 res.render('items', {
//                     page: page, items: data.slice(first, first + nItemsOnOnePage),
//                     prev: (page - 1), next: (page + 1), pages: pagesNum,
//                     ...getUserData(req.user),
//                     isFirstPage: page === 1 ? false : true,
//                     isLastPage: page === pagesNum ? false : true,
//                 });
//             }
//         })
//         .catch(err => {
//             res.end(err.toString());
//         });
// });

// app.get('/items/new', checkAuth, (req, res) => {
//     res.render(`new`, getUserData(req.user));
// });

// app.get('/items/:id', checkAuth, (req, res) => {
//     const id = req.params.id;
//     OrderItem.getById(id)
//         .then(data => {
//             res.render(`item`, { ...data.data, ...getUserData(req.user) });
//         })
//         .catch(err => {
//             res.status(404);
//             res.end(err.toString());
//         });
// });


// /* stuff for insert and update post's */
// function orderItemDataVavidation(data) {
//     if (Number(data.weight_gram) < 0) {
//         throw new Error("Weight is not valid! Try againe!");
//     }
//     if (Number(data.category_id) < 0) { // @todo to real id
//         throw new Error("Category is not valid! Try againe!");
//     }
//     if (Number(data.price) < 0) {
//         throw new Error("Price is not valid! Try againe!");
//     }
// };
// const InsertUpdateEnum = {
//     INSERT: 1,
//     UPDATE: 2,
// };
// Object.freeze(InsertUpdateEnum);
// function makeInsertOrUpdate(reqResDataObj, InsertUpdateEnumVal) {
//     const req = reqResDataObj.req;
//     const res = reqResDataObj.res;

//     const imgFileObject = req.files.img;
//     cloudinary.uploadFile(imgFileObject)
//         .then(data => {
//             try { orderItemDataVavidation(req.body); } catch (err) {
//                 return Promise.reject(err);
//             }
//             const item = new OrderItem({
//                 name: req.body.name,
//                 description: req.body.description,
//                 weight_gram: Number(req.body.weight_gram),
//                 category_id: Number(req.body.category_id),
//                 img: data.url, // @todo (read info at cloudinary.js file)
//                 design_date: new Date().toISOString(),
//                 price: Number(req.body.price),
//             });
//             switch (InsertUpdateEnumVal) {
//                 case InsertUpdateEnum.INSERT: {
//                     return OrderItem.insert(item);
//                 }
//                 case InsertUpdateEnum.UPDATE: {
//                     const id = req.query.id;
//                     return OrderItem.updateById(id, item.data);
//                 }
//                 default: {
//                     return Promise.reject(new Error('Uncorrect value of InsertUpdateEnumVal: ' + InsertUpdateEnumVal));
//                 }
//             }
//         }).then(data => {
//             res.redirect(`/items/${data.id}`);
//         })
//         .catch(err => res.end("Error: " + err.toString()));
// }

// app.post('/new_item', checkAuth, (req, res) => {
//     makeInsertOrUpdate({ req, res }, InsertUpdateEnum.INSERT);
// });

// app.get('/items/:id/edit', checkAuth, (req, res) => {
//     OrderItem.getById(req.params.id)
//         .then(data => res.render(`edit`, { ...data.data, ...getUserData(req.user) }))
//         .catch(err => res.end(err.toString()));
// });

// app.post('/update', checkAuth, (req, res) => {
//     makeInsertOrUpdate({ req, res }, InsertUpdateEnum.UPDATE);
// });

// app.get('/delete', checkAuth, (req, res) => {
//     const id = req.query.id;
//     OrderItem.deleteById(id)
//         .then(() => res.redirect('/items'))
//         .catch(err => res.end(err.toString()));
// });