function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (req.user.role === "client") res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkNotAuth(req, res, next) {
    if (req.user) return res.sendStatus(404); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

module.exports = {
    checkAuth, checkAdmin, checkNotAuth
};