const User = require('../models/user');
const Order = require('../models/order');
const Place = require('../models/place');
const Item = require('../models/item');
const Table = require('../models/table');

/**
 *  Sample Objects
 */
// const sampleEntity = {
//     id: 2242,
//     name: "Test Name JHJH",
//     description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.",
//     small_pic: "/images/food/pizza/1.jpg",
//     big_pic: "/images/restorans/big_pic/final/1.png",
//     price: false,
//     rating: 4,
//     category: {
//         pizza: true,
//     },
//     creation_date: "2018-05-05",
// };
// const sampleEmptyEntity = {
//     // id: 2242,
//     // name: "Test Name JHJH",
//     // description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.",
//     // small_pic: "/images/food/pizza/1.jpg",
//     // price: 0,
//     rating: true,
//     // category: {
//     //     pizza: true,
//     // },
//     creation_date: "2018-05-05",
// };
// const entitiesArr = [
//     sampleEntity,
//     sampleEntity,
//     sampleEntity,
//     sampleEntity,
// ];
// const samplePlaceAditionalObj = {
//     name: "Sample Place Name",
//     id: 444,
// };
// const sampleUserAditionalObj = {
//     id: 4443,
//     first_name: "Bla",
//     last_name: "BLABLABLA",
//     role: "Waiter",
// };
// const sampleTableAditionalObj = {
//     id: 4443,
//     number: 6663,
// };
// const sampleOrder = {
//     id: 66,
//     stage: 1,
//     number: 4444,
//     items: [
//         "Bla bla bka",
//         "bla bla bla 2",
//         "bla-blA-BLA 3",
//     ],
//     price: 56,
//     client_id: 555,
// };
// const orderPart = {
//     part: [
//         sampleOrder,
//         sampleOrder,
//         sampleOrder,
//         sampleOrder,
//         sampleOrder,
//     ]
// };
// const sampleUser = {
//     id: 666,
//     username: "MyHack666",
//     email: "rrr@example.com",
//     first_name: "BlaFirstName",
//     last_name: "BlaLastName",
//     name: "BlaFirstName BlaLastName",
//     registration_date: "1805-05-05",
//     role: {
//         waiter: true,
//     },
//     small_pic: "/images/users/ava/default.png",
// };

/**
 * Partial render
 */

const authInfo = (user) => {
    let result = {
        user_id: user ? user.id : null,
        order_id: (user ? user.order_id : null),
        place_id: (user ? user.place_id: null),
        is_place_admin: false,
        is_system_admin: false,
    };
    if(user) {
        switch(user.role) {
            case "client": {
                result.role = { client: true }; break;
            }
            case "kitchen": {
                result.role = { kitchen: true }; break;
            }
            case "waiter": {
                result.role = { waiter: true }; break;
            }
            case "place_admin": {
                result.role = { place_admin: true }; 
                result.is_place_admin = true;
                break;
            }
            case "system_admin": {
                result.role = { system_admin: true }; 
                result.is_place_admin = true;
                result.is_system_admin = true;
                break;
            }
        }
        if (user.order_id) {
            Order.getById(user.order_id)
            .then(data => Table.getById(data.table))
            .then(data => result.place_id = data.place);
        }
    }

    return result;
};

const pagination = () => {
    return {
        first_page: false,
        last_page: false,
        entities_url_path: "users",
        pages_count: 3,
        pages: [
            {
                number: 1,
                active: false,
            },
            {
                number: 2,
                active: true,
            },
            {
                number: 3,
                active: false,
            }
        ]
    };
};
const partObj = (partArr) => {
    return {
        part: partArr,
    };
};
const categories_list = (name, items) => {
    let obj = {
        categories: [
            {
                category: name,
                items: [],
            }
        ] 
    };
    let k = 0;
    let partArr = [];
    for (let i in items) {
        if (k === 4) {
            k = 0;
            obj.categories[0].items.push(partObj(partArr));
            partArr = [];
        }
        partArr.push(items[i]);
        k++;
    }
    obj.categories[0].items.push(partObj(partArr));
    return obj;
};
const orders_list = (items, p) => {
    let arr = [];
    let k = 0;
    let partArr = [];
    for (let i in items) {
        if (k === p) {
            k = 0;
            arr.push(partObj(partArr));
            partArr = [];
        }
        partArr.push(items[i]);
        k++;
    }
    arr.push(partObj(partArr));
    return arr;
};

/**
 *  Compleate pages
 */
/**
 * Main
 */
const index = (user, data) => {
    return {
        ...authInfo(user),
        ...pagination(),
        // ...categories_list("All", data)
    };
};
const news = (user) => {
    return {
        ...authInfo(user),
        ...pagination(),
    };
};
const about = (user) => {
    return {
        ...authInfo(user),
    };
};
const scan = (user) => {
    return {
        ...authInfo(user),
    };
};

/**
 * Items
 */
const categories = (user) => {
    return {
        ...authInfo(user),
    };
};
const items = (user, data) => {
    return {
        ...authInfo(user),
        ...pagination(),
        ...categories_list("All", data),
    };
};
const item = (user, data) => {
    // console.log(data);
    return {
        ...authInfo(user),
        ...data,
        // place: Place.getTemplate(),
    };
};
const item_edit = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
        // place: Place.getTemplate(),
    };
};
const item_new = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
        new: true,
    };
};

/**
 * Places
 */
const places = (user, data) => {
    return {
        ...authInfo(user),
        ...pagination(),
        ...categories_list("Places", data),
        rating: true,
    };
};
const place = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
        // place: Place.getTemplate(),
    };
};
const place_edit = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
        // place: Place.getTemplate(),
        // staff: [
        //     sampleUserAditionalObj,
        //     sampleUserAditionalObj,
        //     sampleUserAditionalObj,
        // ]
    };
};
const place_new = (user) => {
    return {
        ...authInfo(user),
        place: Place.getTemplate(),
        new: true,
    };
};
const place_tables = (user, data, place_id) => {
    // console.log(place);
    return {
        ...authInfo(user),
        place: {
            id: place_id,
        },
        tables: {
            scheme: "/images/tables/table_scheme/1.jpg",
            tables_list: data,
        }

    };
};

/**
 * Auth
 */
const auth = (user) => {
    return {
        ...authInfo(user),
    };
};

/**
 * Orders
 */
const orders = (user, data) => {
    // console.log(data);
    return {
        ...authInfo(user),
        ...pagination(),
        orders: orders_list(data, 5),
    };
};
const order = (user, data) => { 
    console.log(data);
    return {
        ...authInfo(user),
        ...data,
    };
};

/**
 * Users
 */
const user = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
    };
};
const user_edit = (user, data) => {
    return {
        ...authInfo(user),
        ...data,
    };
};
const users = (user, data) => {
    return {
        ...authInfo(user),
        users: orders_list(data, 4),
    };
};

module.exports = {
    index,
    news,
    about,
    scan,

    categories,
    items,
    item,
    item_edit,
    item_new,

    places,
    place,
    place_edit,
    place_new,
    place_tables,

    auth,

    orders,
    order,

    user,
    user_edit,
    users,

};