const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    login: String, // make unic
    role: Number, // enum
    fullname: String,
    registeredAt: Date,
    avaUrl: String,
    isDisabled: Boolean,
    password: String,
});

class User {
    constructor(data) {
        this.data = {
            id: data.id,
            login: data.login,
            role: data.role,
            fullname: data.fullname,
            registeredAt: data.registeredAt,
            avaUrl: data.avaUrl,
            isDisabled: data.isDisabled,
            password: data.password,
        };

        // this.login = data.login;
        // this.role = data.role;
        // this.fullname = data.fullname;
        // this.registeredAt = data.registeredAt;
        // this.avaUrl = data.avaUrl;
        // this.isDisabled = data.isDisabled;
    }
    static getAll() {
        return User.getDataBaseData().model.find();
    }
    static insert(data) {
        const Model = User.getDataBaseData().model;
        const instanceData = data.data;
        const instance = new Model(instanceData);
        return instance.save();
    }
    static getById(id) {
        return User.getByConditions({
            _id: id,
        });
    }
    static getByLoginAndPassword(login, password) {
        return User.getByConditions({
            login: login,
            password: password,
        });
    }
    static getByConditions(conditions) {
        return User.getDataBaseData().model.findOne(conditions)
        .then((data) => {
            if (data) {
                return new User({
                    id: String(data._id),
                    login: data.login,
                    role: data.role,
                    fullname: data.fullname,
                    registeredAt: data.registeredAt,
                    avaUrl: data.avaUrl,
                    isDisabled: data.isDisabled,
                });
            } else return Promise.reject(new Error("Can't find user by this conditions!"));
        }).catch(err => console.log(err));
    }
    static updateRoleById(id, data) {
        return User.getDataBaseData().model.findByIdAndUpdate(id, {
            $set: {
                role: data.role,
            }
        });
    }
    static updateById(id, data) {
        return User.getDataBaseData().model.findByIdAndUpdate(id, {
            $set: {
                login: data.login,
                role: data.role,
                fullname: data.fullname,
                avaUrl: data.avaUrl,
            }
        });
    }
    static deleteById(id) {
        return User.getDataBaseData().model.findByIdAndDelete(id);
    }
    static getDataBaseData() {
        return {
            schema: UserSchema,
            model: mongoose.model('User', UserSchema),
        };
    }
}

module.exports = User;