const express = require('express');
const url = require('url');
const cloudinary = require('./cloudinary');

// coockies and session
const passport = require('passport');
// const LocalStrategy = require('passport-local').Strategy;
// const cookieParser = require('cookie-parser');
// const session = require('express-session');

const User = require('./models/user');

const router = express.Router();

function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

router.get('/login', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.data.id); 
        return;
    }

    if (!req.query.error) {
        res.render('login');
    } else {
        res.render('login', {
            error: req.query.error,
        });
    }
});

router.get('/register', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.data.id); 
        return;
    }

    if (!req.query.error) {
        res.render('register');
    } else {
        res.render('register', {
            error: req.query.error,
        });
    }
});

router.post('/login', passport.authenticate('local', { failureRedirect: '/auth/login?error=Incorrect+data,+try+again!' }), (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.data.id); 
        return;
    }

    res.redirect('/');
});

router.post('/register', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.data.id); 
        return;
    }

    // @todo server validation
    if (req.body.password !== req.body.confirm_password) {
        // res.render('register', {
        //     error: "Paswords doesn't match each other!"
        // });
        res.redirect(url.format({
            pathname:"/register",
            query: {
               "error": "Paswords doesn't match each other!",
            }
        }));
    } else {
        const imgFileObject = req.files.avaUrl;
        cloudinary.uploadFile(imgFileObject)
        .then(data => {
            // // @todo validation
            // try { orderItemDataVavidation(req.body); } catch (err) { 
            //     return Promise.reject(err);
            // }
            const user = new User({
                login: req.body.login,
                role: 0,
                fullname: req.body.fullname,
                registeredAt: new Date().toISOString(),
                avaUrl: data.url,
                isDisabled: true,
                password: req.body.password,
            });
            // switch (InsertUpdateEnumVal) {
            //     case InsertUpdateEnum.INSERT: {
            //         return OrderItem.insert(item);
            //     }
            //     case InsertUpdateEnum.UPDATE: {
            //         const id = req.query.id;
            //         return OrderItem.updateById(id, item.data);
            //     }
            //     default: {
            //         return Promise.reject(new Error('Uncorrect value of InsertUpdateEnumVal: ' + InsertUpdateEnumVal));
            //     }
            // }
            return User.insert(user);
        }).then(data => {
            // res.redirect(`/users/${data.id}`);
            res.redirect('/auth/login');
        })
        .catch(err => res.end("Error: " + err.toString()));
    }

});

router.get('/logout', checkAuth, (req, res) => {
    req.logout();   
    res.redirect('/');
});


module.exports = router;