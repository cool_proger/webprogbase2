jQuery(document).ready(function () {
    $('.table_qrcode').each(function (index, element) {
    // index (число) - текущий индекс итерации (цикла)
        // данное значение является числом
        // начинается отсчёт с 0 и заканчивается количеству элементов в текущем наборе минус 1
    // element - содержит DOM-ссылку на текущий элемент
    
        const id = $(element).attr('id'); 
        $(element).qrcode({
            text: id
        });
        // $(element).attr('width', 128);
        // $(element).attr('height', 128);

    });

    // jQuery('#qrcode').qrcode({
    //     text: "83392235"
    // });
});