const createObjectURL = (window.URL || window.webkitURL || {}).createObjectURL || function(){};


if(navigator.webkitGetUserMedia !== null) { 
    const options = { 
        video:true, 
        // audio:true 
    }; 
    
    // запрашиваем доступ к веб-камере
    navigator.webkitGetUserMedia(options, 
        function(stream) { 
        // получаем тег video
        const video = document.querySelector('video'); 
        // включаем поток в магический URL
        
        // video.src = window.webkitURL.createObjectURL(stream); 
        // video.src = createObjectURL(stream);
        video.srcObject=stream;
        // video.play();

        }, 
        function(e) { 
            console.log("Error reading from camera: " + e); 
        } 
    ); 
}

const video = document.getElementById('video');

QCodeDecoder()
    .decodeFromVideo(video, function(er,res){
        // console.log(res);
        const decoded_text = document.getElementById('decoded_text');
        decoded_text.innerText = "TableID: " + res;

        const qrcode_decoded_btns = document.getElementsByClassName('qrcode_decoded_btn');

        for (let val of qrcode_decoded_btns) {
            let btn_href_str = val.getAttribute('href');
            switch(val.id) {
                case "qrcode_btn_guest": {
                    btn_href_str = "/scan/guest/" + res;
                    break;
                }
                case "qrcode_btn_auth": {
                    btn_href_str = "/scan/auth/" + res;
                    break;
                }
                case "qrcode_btn_success": {
                    btn_href_str = "/scan/success/" + res;
                    break;
                }
            }
            // btn_href_str += ("?table=" + res + "&");
            val.setAttribute('href', btn_href_str);
            val.hidden = false;
        }
});