const express = require('express');
const url = require('url');
const cloudinary = require('./cloudinary');

const crypto = require('crypto');

// coockies and session
const passport = require('passport');
// const LocalStrategy = require('passport-local').Strategy;
// const cookieParser = require('cookie-parser');
// const session = require('express-session');

const User = require('./models/user');

const router = express.Router();

function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

router.get('/login', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.id); 
        return;
    }

    if (!req.query.error) {
        res.render('login');
    } else {
        res.render('login', {
            error: req.query.error,
        });
    }
});

router.get('/register', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.id); 
        return;
    }

    if (!req.query.error) {
        res.render('register');
    } else {
        res.render('register', {
            error: req.query.error,
        });
    }
});

router.post('/login', passport.authenticate('local', { failureRedirect: '/auth/login?error=Incorrect+data,+try+again!' }), (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.id); 
        return;
    }

    res.redirect('/');
});

router.post('/register', (req, res) => {
    if (req.user) {
        res.redirect('/users/' + req.user.id); 
        return;
    }

    // @todo server validation
    if (req.body.password !== req.body.confirm_password) {
        // res.render('register', {
        //     error: "Paswords doesn't match each other!"
        // });
        res.redirect(url.format({
            pathname:"/auth/register",
            query: {
               "error": "Paswords doesn't match each other!",
            }
        }));
    } else {
        const user = new User({
            username: req.body.username,
            // role: "client",
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            registration_date: new Date().toISOString(),
            // small_pic: data.small_pic,
            password_hesh: crypto.createHash('md5').update(req.body.password).digest('hex'),
        });
        User.insert(user)
        .then(data => {
            // res.redirect(`/users/${id}`);
            res.redirect('/auth');
        })
        .catch(err => res.end("Error: " + err.toString()));
    }

});

router.get('/logout', checkAuth, (req, res) => {
    req.logout();   
    res.redirect('/');
});


module.exports = router;