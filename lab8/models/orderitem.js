const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const ObjectId = mongoose.Schema.Types.ObjectId;

const OrderItemSchema = new Schema({
    name: String,
    description: String,
    weight_gram: Number,
    category_id: Number, // @todo make enum
    img: String,
    design_date: Date,
    price: Number,
});

class OrderItem {
    constructor(data) {
        this.data = {
            id: data.id,
            name: data.name,
            description: data.description,
            weight_gram: data.weight_gram,
            category_id: data.category_id,
            img: data.img,
            design_date: data.design_date,
            price: data.price,
        };
    }
    static insert(data) {
        const Model = OrderItem.getDataBaseData().model;
        const instanceData = data.data;
        const instance = new Model(instanceData);
        return instance.save();
    }
    static getById(id) {
        return OrderItem.getDataBaseData().model.findById(id)
            .then((data) => {
                if (data) {
                    return new OrderItem({
                        id: String(data._id),
                        name: data.name,
                        description: data.description,
                        weight_gram: data.weight_gram,
                        category_id: data.category_id,
                        img: data.img,
                        design_date: data.design_date,
                        price: data.price,
                    });
                }
            });
    }
    static updateById(id, data) {
        return OrderItem.getDataBaseData().model.findByIdAndUpdate(id, {
            $set: {
                ...data
            }
        });
    }
    static getByConditions(conditions) {
        return OrderItem.getDataBaseData().model.find(conditions);
    }
    static deleteById(id) {
        return OrderItem.getDataBaseData().model.findByIdAndDelete(id);
    }
    static getAll() {
        return OrderItem.getDataBaseData().model.find();
    }
    static getDataBaseData() {
        return {
            schema: OrderItemSchema,
            model: mongoose.model('OrderItem', OrderItemSchema),
        };
    }
}

module.exports = OrderItem;