const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = require('./user');
const OrderItem = require('./orderitem');

const OrderSchema = new Schema({
    customer: User.schema, // or ref: 'User' @todo ?
    items: [OrderItem],
});

class Order {
    constructor(data) {
        this.data = {
            id: data.id,
            customer: data.customer,
            items: data.items, // item.id only 
        };
    }
    static insert(data) {
        const Model = Order.getDataBaseData().model;
        const instanceData = data.data;
        const instance = new Model(instanceData);
        return instance.save();
    }
    static getByConditions(conditions) {
        return Order.getDataBaseData().model.find(conditions);
    }
    static getById(id) {
        return Order.getDataBaseData().model.findById(id)
            .then((data) => {
                return new Order({
                    id: String(data._id),
                    customer: data.customer,
                    items: data.items,
                });
            });
    }
    static updateById(id, data) {
        return Order.getDataBaseData().model.findByIdAndUpdate(id, {
            $set: {
                items: data.items,
            }
        });
    }
    static deleteById(id) {
        return Order.getDataBaseData().model.findByIdAndDelete(id);
    }
    static getDataBaseData() {
        return {
            schema: OrderSchema,
            model: mongoose.model('Order', OrderSchema),
        };
    }
}

module.exports = Order;