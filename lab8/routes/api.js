const express = require('express');

const User = require('../models/user');
const OrderItem = require('../models/orderitem');
const Order = require('../models/order');

const router = express.Router();

// const authOnly = express.basicAuth(function(user, pass, next) {
//     var result = (user === 'testUser' && pass === 'testPass');
//     next(null, result);
//     req.user
// });

// const adminOnly = express.basicAuth(function(user, pass, next) {
//     var result = (user === 'testUser' && pass === 'testPass');
//     next(null, result);
// });


router.get('/', (req, res) => {
    const jsonStr = JSON.stringify({});
    res.setHeader('Content-Type', 'application/json');
    res.end(jsonStr);
});

function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

router.get('/me', checkAuth, (req, res) => {
    const currUserId = req.user.data.id;
    User.getById(currUserId)
        .then(data => {
            const jsonStrUser = JSON.stringify(data);
            res.setHeader('Content-Type', 'application/json');
            res.end(jsonStrUser);
        })
        .catch(err => {
            res.status(404);
            res.end(err.toString());
        });
});

/* USERS */

router.get('/users', (req, res) => {
    const page = Number(req.query.page ? req.query.page : 1);
    const nItemsOnOnePage = 5;
    if (req.query.page) {
        delete req.query.page;
    }

    User.getByConditions(...req.query)
        .then(data => {
            const first = (page - 1) * nItemsOnOnePage;
            if (first < 0 || data.length - 1 < first) {
                res.sendStatus(404);
            } else {
                const pagesNum = Math.ceil(data.length / nItemsOnOnePage);
                const json_str = JSON.stringify({
                    page: page, 
                    pages: pagesNum,
                    items: data.slice(first, first + nItemsOnOnePage),
                })
            }
            res.setHeader('Content-Type', 'application/json');
            res.end(json_str);
        }).catch(err => {
            res.status(404);
            res.end(err.toString());
        });

router.get('/users/:id', (req, res) => {
    const id = req.params.id;
    User.getById(id)
        .then(data => {
            const jsonStrUser = JSON.stringify(data);
            res.setHeader('Content-Type', 'application/json');
            res.end(jsonStrUser);
        })
        .catch(err => {
            res.status(404);
            res.end(err.toString());
        });
});

router.post('/users/register', (req, res) => {
    if (req.body.password !== req.body.confirm_password) {
        res.redirect(url.format({
            pathname:"/register",
            query: {
               "error": "Paswords doesn't match each other!",
            }
        }));
    } else {
        const user = new User({
            login: req.body.login,
            role: 0,
            fullname: req.body.fullname,
            registeredAt: new Date().toISOString(),
            avaUrl: body.avaUrl,
            isDisabled: true,
            password: req.body.password,
        });
        User.insert(user)
        .then(() => res.end("Sucess!"))
        .catch(err => res.end(err.toString()))
    }
});

router.post('/users/:id/update', (req, res) => {
    User.updateById(req.params.id, {
        ...req.body
    }).then(() => res.end("Success!"))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/users/:id/delete', (req, res) => {
    const id = Number(req.params.id);
    User.deleteById(id)
    .then(() => res.end("Success!"))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/', (req, res) => {
    const jsonStr = JSON.stringify({});
    res.setHeader('Content-Type', 'application/json');
    res.end(jsonStr);
});

/* ITEMS */
function orderItemDataVavidation(data) {
    if (Number(data.weight_gram) < 0) {
        throw new Error("Weight is not valid! Try againe!");
    }
    if (Number(data.category_id) < 0) { // @todo to real id
        throw new Error("Category is not valid! Try againe!");
    }
    if (Number(data.price) < 0) {
        throw new Error("Price is not valid! Try againe!");
    }
};
const InsertUpdateEnum = {
    INSERT: 1,
    UPDATE: 2,
};
Object.freeze(InsertUpdateEnum);
function makeInsertOrUpdate(reqResDataObj, InsertUpdateEnumVal) {
    const req = reqResDataObj.req;
    const res = reqResDataObj.res;

    try { orderItemDataVavidation(req.body); } catch (err) {
        return Promise.reject(err);
    }
    const item = new OrderItem({
        name: req.body.name,
        description: req.body.description,
        weight_gram: Number(req.body.weight_gram),
        category_id: Number(req.body.category_id),
        img: req.body.url, // @todo (read info at cloudinary.js file)
        design_date: new Date().toISOString(),
        price: Number(req.body.price),
    });
    let promise;
    switch (InsertUpdateEnumVal) {
        case InsertUpdateEnum.INSERT: {
            promise = OrderItem.insert(item);
        }
        case InsertUpdateEnum.UPDATE: {
            const id = req.query.id;
            promise = OrderItem.updateById(id, item.data);
        }
        default: {
            promise = Promise.reject(new Error('Uncorrect value of InsertUpdateEnumVal: ' + InsertUpdateEnumVal));
        }
    }
    promise.then(data => {
        res.end("Success!");
    })
    .catch(err => res.end("Error: " + err.toString()));
}

router.get('/items/new', (req, res) => {
    makeInsertOrUpdate({req, res}, InsertUpdateEnum.INSERT);
});

router.get('/items', (req, res) => {
    const page = Number(req.query.page ? req.query.page : 1);
    const nItemsOnOnePage = 5;
    if (req.query.page) {
        delete req.query.page;
    }

    OrderItem.getByConditions(req.query)
    .then(data => {
        const first = (page - 1) * nItemsOnOnePage;
        if (first < 0 || data.length - 1 < first) {
            res.sendStatus(404);
        } else {
            const pagesNum = Math.ceil(data.length / nItemsOnOnePage);
            const json_str = JSON.stringify({
                page: page, 
                pages: pagesNum,
                items: data.slice(first, first + nItemsOnOnePage),
            });

            res.setHeader('Content-Type', 'application/json');
            res.end(json_str);
            
        }
    }).catch(err => {
        res.status(404);
        res.end(err.toString());
    });
});

router.get('/items/:id', (req, res) => {
    const id = req.params.id;
    OrderItem.getById(id)
        .then(data => {
            const jsonStrUser = JSON.stringify(data);
            res.setHeader('Content-Type', 'application/json');
            res.end(jsonStrUser);
        })
        .catch(err => {
            res.status(404);
            res.end(err.toString());
        });
});

router.post('/items/:id/update', (req, res) => {
    OrderItem.updateById(req.params.id, {
        ...req.body
    }).then(() => res.end("Success!"))
    .catch(err => res.end("Error: " + err.toString()));
});

router.get('/items/:id/delete', (req, res) => {
    const id = Number(req.params.id);
    OrderItem.deleteById(id)
    .then(() => res.end("Success!"))
    .catch(err => res.end("Error: " + err.toString()));
});

/* Orders */

router.post('/orders/new', (req, res) => {
    Order.insert({
        customer: req.body.customer,
        item: req.body.items,
    })
    .then(() => {
        res.end("Success!");
    })
    .catch(err => res.end(err.toString()));
});

router.get('/orders', (req, res) => {
    Order.getByConditions(req.query)
    .then(data => {
        const jsonStr = JSON.stringify(data);
        res.setHeader('Content-Type', 'application/json');
        res.end(jsonStr);
    })
    .catch(err => res.end(err.toString()));
});

router.get('/orders/:id', (req, res) => {
    const id = Number(req.params.id);
    Order.getById(id)
    .then(data => {
        const jsonStr = JSON.stringify(data);
        res.setHeader('Content-Type', 'application/json');
        res.end(jsonStr);
    })
    .catch(err => res.end(err.toString()));
});

router.get('/orders/:id/update', (req, res) => {
    Order.updateById(req.params.id, {
        items: req.body.items,
    }).then(() => res.end("Success!"))
    .catch(err => res.end("Error: " + err.toString()));
});


router.get('/orders/:id/delete', (req, res) => {
    Order.deleteById(req.params.id)
    .then(() => res.end("Success!"))
    .catch(err => res.end(err.toString()));
});

module.exports = router;